import fetch from 'node-fetch';
import { promises as fs } from 'fs';

const url = "https://ru.wikipedia.org/wiki/TypeScript";

const writeFile = async (outputPath: string, data: string) => {
    await fs.writeFile(outputPath, data);
};

fetch(url)
    .then(response => {
        response.text().then(function(text) {
            writeFile('./dist/2.txt', text)
        })
    })
    .catch((err) => console.log("Error", err))


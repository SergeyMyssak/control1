import fetch from 'node-fetch';
import { promises as fs } from 'fs';

const url = "https://ru.wikipedia.org/wiki/TypeScript";

const readFile = async (inputPath: string) => {
    return await fs.readFile(inputPath, 'utf-8');
};

const removeScripts = (s: string) => {
    var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    while (SCRIPT_REGEX.test(s)) {
        s = s.replace(SCRIPT_REGEX, "");
    }
    return s;
};

const processText = (data: string) => {
    const dataWithoutScripts = removeScripts(data);
    const dataWithoutHtmlTags = dataWithoutScripts.replace(/<[^>]+>/g, '');
    const dataWitoutLinebreaks = dataWithoutHtmlTags.replace(/[\r\n]+/gm, '');
    const dataWitoutLongGap = dataWitoutLinebreaks.replace(/  +/g, ' ');
    const dataWitoutTabs = dataWitoutLongGap.replace(/\t+/g, ' ');

    const arr = dataWitoutTabs.split(' ');
    let res = 0;

    arr.forEach((word) => {
        const fWord = word.toLowerCase();

        for (let i = 0; i < fWord.length; i++) {
            const letter = fWord[i];
            const newWord = fWord.slice(i + 1, fWord.length);
            if (newWord.indexOf(letter) !== -1) {
                res++;
                break;
            }
        }
    });
}

const main = async () => {
    const text = await readFile('./dist/2.txt');
    processText(text);
}

main();
